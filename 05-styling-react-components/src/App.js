import React, {Component} from 'react';
import classes from './App.module.css';
// import Radium, { StyleRoot } from "radium";
import Person  from './Person/Person'

class App extends Component {
    state = {
        persons: [
            { id: 'asa01', name: 'Max', age: 28},
            { id: 'asa02', name: 'Manu', age: 29},
            { id: 'asa03', name: 'Stephanie', age: 26},
        ],
        otherState: 'some other value',
        showPersons: false,
    }

    nameChangedHandler = (event, id) => {
        const personIndex = this.state.persons.findIndex(p => {
            return p.id === id;
        })
        const person = {
            ...this.state.persons[personIndex]
        }

        // const person = Object.assign({}, this.state.persons[personIndex])
        person.name = event.target.value

        const persons = [...this.state.persons]
        persons[personIndex] = person

        this.setState({
            persons: persons
        })
    }

    deletePersonHandler = (personIndex) => {
        // const persons = this.state.persons.slice()
        const persons = [...this.state.persons]
        persons.splice(personIndex, 1)
        this.setState({persons: persons})
    }

    togglePersonsHandler = () => {
        const doesShow = this.state.showPersons;
        this.setState({
            showPersons: !doesShow
        })
    }

    render() {
        // const style = {
        //     backgroundColor: 'green',
        //     color: 'white',
        //     font: 'inherit',
        //     border: '1px solid blue',
        //     padding: '8px',
        //     cursor: 'pointer',
        //     // ':hover': {
        //     //     backgroundColor: 'lightgreen',
        //     //     color: 'black',
        //     // }
        // }
        let persons = null
        let btnClass = ''

        if (this.state.showPersons) {
            persons = (
                <div>
                    {this.state.persons.map((person, index) => {
                        return <Person
                            // click={this.deletePersonHandler.bind(this, index)} - alternative
                            click={() => this.deletePersonHandler(index)}
                            name={person.name}
                            age={person.age}
                            key={person.id}
                            changed={(event) => this.nameChangedHandler(event, person.id)}
                        />
                    })}

                </div>
            );

            btnClass = classes.Red
            // style.backgroundColor = 'red' // dynamic changing of color
            // style[':hover'] = {
            //     backgroundColor: 'salmon',
            //     color: 'black',
            // }
        }

        const AssignedClasses = []
        if (this.state.persons.length <= 2) {
            AssignedClasses.push (classes.red) // classes = ['red']
        }
        if (this.state.persons.length <= 1) {
            AssignedClasses.push (classes.bold) // classes = ['red', 'bold']
        }

        console.log(AssignedClasses)

        return (
            {/*<StyleRoot>*/},
                <div className={classes.App}>
                    <h1>Hi, I am React</h1>
                    <p className={AssignedClasses.join(' ')}>This is really working!</p>
                    <button
                        // style={style}
                        className={btnClass}
                        onClick={this.togglePersonsHandler}>Toggle Persons</button>
                    {persons}
                </div>
            // </StyleRoot>
        );
        // return React.createElement('div', {className: 'App'}, React.createElement('h1',null, 'Hi, I\'m React'))
    }
}

// export default Radium(App); // Higher order Component
export default App;
