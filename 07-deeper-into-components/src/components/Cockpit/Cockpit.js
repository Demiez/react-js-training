import React from 'react'
import classes from "./Cockpit.module.css";
import Aux from '../hoc/AuxComponent'

const cockpit = (props) => {
    const AssignedClasses = []
    let btnClass = classes.Button

    if (props.showPersons) {
        btnClass = [classes.Button, classes.Red].join(' ')
    }


    if (props.persons.length <= 2) {
        AssignedClasses.push (classes.red) // classes = ['red']
    }
    if (props.persons.length <= 1) {
        AssignedClasses.push (classes.bold) // classes = ['red', 'bold']
    }
    return (
        <Aux>
            <h1>{props.appTitle}</h1>
            <p className={AssignedClasses.join(' ')}>This is really working!</p>
            <button
                className={btnClass}
                onClick={props.clicked}>Toggle Persons</button>
        </Aux>

    )
}

export default cockpit;
