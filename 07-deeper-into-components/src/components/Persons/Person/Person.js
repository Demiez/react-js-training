import React from 'react'
import PropTypes from 'prop-types'
// import Radium from "radium";

import classes from './Person.module.css'
// import WithClass from '../../hoc/WithClass'
import Aux from '../../hoc/AuxComponent'
import withClass from '../../hoc/withClassVer2'

class Person extends React.Component {
    constructor(props) {
        super(props)
        console.log('[Person.js] Inside Constructor', props)
    }

    UNSAFE_componentWillMount() {
        console.log('[Person.js] Inside componentWillMount()')
    }

    componentDidMount() {
        console.log('[Person.js] Inside componentDidMount()')
        if (this.props.position === 1) {
            console.log(this.inputElement)
            this.inputElement.focus()
        }
    }
    render () {
        console.log('[Person.js] Inside render()')

        return (
                <Aux>
                {/*<WithClass classes={classes.Person}>*/}
                    <p onClick={this.props.click}>I'm {this.props.name} and I am {this.props.age} years old</p>
                    <p>{this.props.children}</p>
                    <input
                        ref={(inp) => { // refs are only used in stateful components
                            this.inputElement = inp
                        }}
                        type="text"
                        onChange={this.props.changed}
                        value={this.props.name}/>
                {/*</WithClass>*/}
                </Aux>
        )
    //     return [
    //         <p key='1' onClick={this.props.click}>I'm {this.props.name} and I am {this.props.age} years old</p>,
    //         <p key='2'>{this.props.children}</p>,
    // <input key='3' type="text" onChange={this.props.changed} value={this.props.name}/>
    //     ]
    }
}

Person.propTypes = {
    click: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    changed: PropTypes.func,
}

export default withClass(Person, classes.Person);
