import React, {Component} from 'react'

/*const withClass = (WrappedComponet, className) => { // get configuration not props, it is not a functional component, it is a function itself, because it doesn't return JSX it returns another function
    return (props) => (
        <div className={className}>
            <WrappedComponet {...props}/>
        </div>
    )
} */
const withClass = (WrappedComponet, className) => {
    return class extends Component { // Anonymous class and we can pass a stateful component
        render () {
            return (
                <div className={className}>
                    <WrappedComponet {...this.props}/>
                </div>
            )
        }
    }
}

export default withClass;
